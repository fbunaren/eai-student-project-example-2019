package com.adf.tugasakhir.dataclass;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity @Getter @Setter
public class Paper {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Column(name = "title")
    private String title;

    @NotNull
    @Column(name = "abstrak")
    private String abstrak;

    @NotNull
    @Column(name = "url")
    private String url;

    public Paper(String title, String url, String abstrak) {
        this.title = title;
        this.abstrak = abstrak;
        this.url = url;
    }

    @Override
    public String toString() {
        return "Paper{" + "title='" + title + '\'' + ", abstrak='" + abstrak + '\'' + ", url='" + url + '\'' + '}';
    }
}
