# EAI Course Student Project 2019

## Work Checklist
- [x] Fork the project to your own GitLab project namespace

- [x] Setup new Heroku app and configure CI/CD variables in your own fork

- [x] Clone the fork codebase to your local machine

- [x] Begin working on on the mandatory tasks

- [x] Do not forget to push latest state of your work to the online Git repository

- [x] Submit the following on Scele:

        URL to your GitLab project (make sure it is public!)

## CI/CD Variables

You need to prepare the following variables on your GitLab CI/CD project:

- `HEROKU_APP_NAME` - Name of the app on your Heroku account
- `HEROKU_API_KEY` - The API key required for deploying the app via your Heroku account

## Original Contributors

The original (`upstream`) repository can be found at:
https://gitlab.com/tengkuizdihar/adftugasakhir

- [Adrika Novrialdi](https://gitlab.com/adrika-novrialdi)
- [Farhan Nurdiatama Pakaya](https://gitlab.com/farhan.np9)
- [Tengku Izdihar](https://gitlab.com/tengkuizdihar)