# CSCM602023 Advanced Programming (KKI) - 2020 - Final Programming Exam

> TODO: Replace this line with your full name and student ID (NPM)

TODO: Write the answers related to the programming exam here.

## 12 Factor Issues

- [x] Config

    In the `application.properties` , the API Key for `text.analytics.api.key` is hardcoded, which violates the config part of 12 Factor.
    Config should be stored in Environment Variable for security reason. 
    To fix this issue, I made envrionment variable in `Heroku`, `GitLab` and `IntelliJ` for `text.analytics.api.key`
    with the name `TEXT_ANALYTICS_API_KEY`.
    
    I also found another API Key Stored in the `TextAnalysisServiceImpl.java`,
    I changed the hardcoded API into `System.getenv("TEXT_ANALYTICS_API_KEY");`
    
    
    
## Screenshot Part 2
![prometheus](img.png)